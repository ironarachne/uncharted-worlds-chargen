<?php

namespace App;


class Asset
{
    public string $name;
    public int $class;
    public string $description;
    public string $type;
    public array $upgrades;

    public function __construct() {
        $this->name = '';
        $this->class = 0;
        $this->description = '';
        $this->type = '';
        $this->upgrades = [];
    }
}
