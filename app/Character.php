<?php

namespace App;


class Character
{
    public array $skills;
    public array $descriptors;

    public int $mettle;
    public int $physique;
    public int $expertise;
    public int $influence;
    public int $interface;

    public Origin $origin;

    public Career $careerOne;
    public Career $careerTwo;

    public array $assets;

    public function __construct()
    {
        $this->assets = [];
        $this->descriptors = [];
        $this->skills = [];
    }

}
