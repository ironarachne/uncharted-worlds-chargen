<?php

namespace App;


class Origin
{
    public array $skills;
    public array $descriptors;
    public string $name;

    public function __construct()
    {
        $this->skills = [];
        $this->descriptors = [];
        $this->name = '';
    }
}
