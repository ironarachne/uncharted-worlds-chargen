<?php

namespace App;


class Career
{
    public array $skills;
    public array $descriptors;
    public string $name;
    public array $advancements;
    public array $workspaces;

    public function __construct() {
        $this->skills = [];
        $this->descriptors = [];
        $this->name = '';
        $this->advancements = [];
        $this->workspaces = [];
    }
}
