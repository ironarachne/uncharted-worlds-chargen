<?php

namespace App;


class Skill
{
    public string $name;
    public string $text;

    public function __construct($name, $text)
    {
        $this->name = $name;
        $this->text = $text;
    }
}
