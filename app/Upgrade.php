<?php

namespace App;


class Upgrade
{
    public string $name;
    public string $effect;

    public function __construct($name, $effect)
    {
        $this->name = $name;
        $this->effect = $effect;
    }
}
