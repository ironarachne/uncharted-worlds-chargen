<!DOCTYPE html>
<html>
<head>
    <title>Uncharted Worlds Character Generator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css">
    <script async defer data-website-id="06ec131e-7511-4d46-b08a-cd6ba7b776f9" src="https://umami.benovermyer.com/umami.js"></script>
</head>
<body>
<section class="section">
    <div class="container">
        <div class="columns">
            @yield('content')
        </div>
    </div>
</section>
</body>
</html>
